#!/usr/bin/env python


import os
import smtplib
import ssl
import webbrowser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import yaml
from jinja2 import Environment, FileSystemLoader, select_autoescape
from rich import print


email_path = ""


def render_template(args: dict) -> str:
    env = Environment(
        loader=FileSystemLoader('.'),
        autoescape=select_autoescape(['html'])
    )
    template = env.get_template('template.j2')
    html = template.render(**args)

    with open("email.html", "w") as file:
        file.write(html)

    # for debugging
    global email_path
    email_path = os.path.abspath("email.html")

    return html


def sendmail(html):
    mail = "marco.heins@posteo.de"
    smtp = smtplib.SMTP_SSL("posteo.de", 465, ssl.create_default_context())
    with open("../EMAIL_PWD") as email_pwd_file:
        email_pwd = email_pwd_file.read().rsplit()[0]
    smtp.login(mail, email_pwd)

    msg = MIMEMultipart("alternative")
    msg.attach(MIMEText("Empty", 'plain'))
    msg["Subject"] = "gg.dealz prices"
    msg["From"] = mail
    msg["To"] = mail
    msg.attach(MIMEText(html, 'html'))

    smtp.sendmail(mail, mail, msg.as_string())


if __name__ == '__main__':
    with open("prices.yaml") as prices_yaml:
        game_prices = yaml.safe_load(prices_yaml)
    render_template({"game_prices": game_prices})
    webbrowser.open(f"file://{email_path}")
    with open("email.html") as email_html:
        sendmail(email_html.read())
