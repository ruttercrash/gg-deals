#!/usr/bin/env python


""" Read game names from text file, browse gg.deals und fetch lowest current price
    as well as 3- & 12-month- & all-time-lowest prices
"""


from dataclasses import dataclass, field
import os.path

from playwright.sync_api import sync_playwright, Page
from rich.console import Console
import yaml

from sendmail import render_template, sendmail


TESTING = False
HEADLESS = True
GAMES_LIST = "games.txt"

if TESTING:
    import webbrowser
    GAMES_LIST = "dummy.txt"

console = Console()
email_path = os.path.abspath("email.html")


XPATH = {
    "price_history": '//*[@id="tab-price-history"]/a',
    "prices_by_time": '//*[@id="game-lowest-tab-trigger-time"]',
    "prices": {
        "current": '//*[@id="game-price-history-ranking"]/div[3]/a/span[2]/span[1]/span',
        "low_3_month": '//*[@id="game-lowest-tab-time"]/div[1]/div[1]/div[2]/span/span',
        "low_12_month": '//*[@id="game-lowest-tab-time"]/div[2]/div[1]/div[2]/span/span',
        "low_all_time": '//*[@id="game-lowest-tab-time"]/div[3]/div[1]/div[2]/span/span',
    }
}


@dataclass
class Game:
    name: str
    price: dict = field(default_factory=lambda: dict())

    def parse_prices(self, page: Page):
        """ Fetch prices: current, 3 & 12 month & all-time lowest from viewport"""
        for timestamp, xpath in XPATH["prices"].items():
            price = page.locator(xpath).inner_text().split("€")[0]
            price = price[1:] if price.startswith("~") else price
            price = "0" if price.find("Free") >= 0 else price
            price = float(price.replace(",", "."))

            self.price[timestamp] = price

        for timestamp in list(self.price.keys())[1:]:
            self.price[timestamp] = float(f"{self.price[timestamp] - self.price['current']:.2f}")

    def to_dict(self) -> dict:
        return {
            "name": self.name,
            "price": self.price
        }


def read_games_list(filename: str) -> list[str]:
    with open(filename) as games_list:
        games = games_list.readlines()
    games = [game.rstrip() for game in games]  # remove /n

    return games


def click_after_wait(page: Page, selector: str):
    page.wait_for_selector(selector)
    page.click(selector)


def select_prices_by_time(page: Page, game_name: str):
    """ Surf to game page und bring prices to fetch into viewport"""

    page.goto(f"https://gg.deals/game/{game_name}")
    click_after_wait(page, XPATH["price_history"])
    click_after_wait(page, XPATH["prices_by_time"])


def set_price_color(current_game: Game, style: str) -> str:
    """ red <= 3 month low, yellow <= 12 month low, green <= all-time low
    """
    price_color = style
    if current_game.price['low_3_month'] >= 0:
        price_color = "bright_red"
    if current_game.price['low_12_month'] >= 0:
        price_color = "yellow3"
    if current_game.price['low_all_time'] >= 0:
        price_color = "green3"

    return price_color


def toggle_font_color(current: str) -> str:
    return "bright_black" if current == "bright_white" else "bright_white"


def display_prices(current_game: Game, price_today_color: str, font_color: str) -> None:
    console.print(f"[{font_color}]"
                  f"{current_game.name[:29]:30}     "
                  f"today: [{price_today_color}]{current_game.price['current']:6}[/{price_today_color}]     "
                  f"[bright_red]3m[/]: {current_game.price['low_3_month']:6.2f}     "
                  f"[yellow3]12m[/]: {current_game.price['low_12_month']:6.2f}     "
                  f"[green3]all[/]: {current_game.price['low_all_time']:6.2f}"
                  f"[/{font_color}]",
                  highlight=False,

                  )


def main():
    game_names: list[str] = read_games_list(GAMES_LIST)
    font_color = "bright_white"

    with sync_playwright() as pw:
        chrome = pw.chromium.launch(headless=HEADLESS)
        context = chrome.new_context(viewport={"width": 1920, "height": 1080})
        page = context.new_page()

        game_prices = []
        for game_name in game_names:
            select_prices_by_time(page, game_name)

            game = Game(game_name)
            game.parse_prices(page)

            price_today_color = set_price_color(game, font_color)
            display_prices(game, price_today_color, font_color)
            font_color = toggle_font_color(font_color)
            game_prices.append(game.to_dict())

    with open("prices.yaml", "w") as prices_yaml:
        yaml.dump(game_prices, prices_yaml)

    html = render_template({"game_prices": game_prices})
    #sendmail(html)


if __name__ == '__main__':
    main()
    if TESTING:
        webbrowser.open(f"file://{email_path}")

